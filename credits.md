# Bussard credits

Original code, art, and prose copyright © 2015-2017 Phil Hagelberg and contributors
https://gitlab.com/technomancy/bussard/graphs/master

Distributed under the GNU General Public License version 3 or later; see LICENSE

## 3rd-party code (direct dependencies listed; LÖVE has additional indirect ones)

* LÖVE copyright © 2006-2016 LOVE Development Team, distributed under the zlib license
  https://love2d.org
* lume copyright © 2015 rxi, distributed under the MIT license
  https://github.com/rxi/lume
* serpent copyright © 2011-2013 Paul Kulchenko, distributed under the MIT license
  https://github.com/pkulchenko/serpent
* utf8.lua copyright © 2016 Stepets, distributed under the MIT license
  https://github.com/Stepets/utf8.lua
* l2l copyright © 2012-2015, Eric Man and contributors, distributed under 2-clause BSD license
  https://github.com/meric/l2l
* globtopattern copyright © 2011-2012 David Manura, distributed under the MIT license
  https://github.com/davidm/lua-glob-pattern

## 3rd-party art

* Some planet graphics copyright © 2013 Rawdanitsu, CC0 licensed
  http://opengameart.org/content/planets-and-stars-set-high-res
* Other planet graphics copyright © 2013 GM Shaber, CC-BY licensed
  http://opengameart.org/content/27-planets-in-hi-res
* Solar system graphics copyright © 2013 Inove, CC-BY 4.0 licensed
  https://www.solarsystemscope.com/textures (based on NASA imagery)
* Station/portal graphics copyright © 2014 MillionthVector, CC-BY licensed
  https://millionthvector.blogspot.de/p/free-sprites_12.html
* Asteroid graphics copyright © 2014 para, CC0 licensed
  http://opengameart.org/content/low-poly-rocks
* Inconsolata font copyright © 2006 Raph Levien
  Released under the SIL Open Font License
  https://www.google.com/fonts/specimen/Inconsolata
* Deja Vu Sans Mono font copyright © 2004-2016 DejaVu fonts team
  Released under Bitstream Vera Fonts Copyright
  https://dejavu-fonts.github.io/
* Noto Thai copyright © 2016 Google
  Released under the SIL Open Font License 1.1
  https://www.google.com/get/noto/

## 3rd-party prose

* Lua Programming book copyright © 2013-2016 Mark Otaris and Wikibooks contributors,
  distributed under the Creative Commons Attribution-ShareAlike license
  https://en.wikibooks.org/wiki/Lua_Programming

## Influences

* Escape Velocity (gameplay)
* Kerbal Space Program (mechanics)
* Marathon Trilogy (story)
* A Fire upon the Deep (story)
* The Lathe of Heaven (story, philosophy)
* Mindstorms (philosophy)
* GNU Emacs (architecture)
* Unix (architecture)
* Atomic Rockets (science)
* Planescape: Torment (story, gameplay)
* Meditations on Moloch (philosophy)
* TIS-100 (puzzle design)
* Duskers (gameplay)

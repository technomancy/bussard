-- Wrapping access to the real filesystem to check permissions and such.
local globtopattern = require("lib.globtopattern").globtopattern
local lume = require("lume")
local lfs = require("love.filesystem")
local seed_data = {} -- require("data.seed")

local fs = {}

local realpath = function(path, cwd)
   -- fs.normalize always returns a value starting with /
   return "fs/" .. fs.hostname .. fs.normalize(path, cwd)
end

-- Actually returns both the dirname and the basename.
-- for instance, "/path/to/file" returns "/path/to" and "file"
fs.dirname = function(path)
   local t = lume.split(path, "/")
   local basename = t[#t]
   table.remove(t, #t)

   return "/" .. table.concat(t, "/"), basename
end

fs.expand_globs = function(input, env)
   local segments = lume.split(input, " ")
   local expand = function(token)
      local matches = {}
      if(token:match("*")) then
         local pattern = globtopattern(token)
         -- TODO: glob into directories
         for _,name in ipairs(lfs.getDirectoryItems(realpath(env.CWD))) do
            if(name:match(pattern) and not name:match("^_")) then
               table.insert(matches, name)
            end
         end
         return table.concat(matches, " ")
      else
         return token
      end
   end
   return table.concat(lume.map(segments, expand), " ")
end

fs.ls = function(path, cwd)
   local items = lfs.getDirectoryItems(realpath(path, cwd))
   local no_meta = function(entry)
      return not entry:match("/_meta$") and not entry:match("^_meta$")
   end
   return lume.filter(items, no_meta)
end

fs.read = function(path, cwd)
   assert(not path:match("/_meta$"), "Don't mess with fs meta!")
   local contents, _ = lfs.read(realpath(path, cwd))
   return contents
end

fs.write = function(path, cwd, content)
   assert(not path:match("/_meta$"), "Don't mess with fs meta!")
   return lfs.write(realpath(path, cwd), content)
end

fs.chown = function(path, cwd, user, group, group_write)
   assert(lfs.write(realpath(path .. "/_meta", cwd),
                    lume.serialize({user=user,group=group,
                                    group_write=group_write})))
end

fs.exists = function(path, cwd)
   return lfs.getInfo(realpath(path, cwd))
end

fs.isdir = function(path, cwd)
   local info = lfs.getInfo(realpath(path, cwd))
   return info and info.type == "directory"
end

fs.rm = function(path, cwd)
   lfs.remove(realpath(path, cwd))
end

fs.mkdir = function(path, cwd, env)
   if(fs.isdir(path, cwd)) then return end
   -- TODO: create dir meta for parents that lack it
   lfs.createDirectory(realpath(path, cwd))
   if(env) then
      fs.chown(path, cwd, env.USER, env.USER)
   else
      fs.chown(path, cwd, "root", "root")
   end
end

fs.dir_meta = function(dir, cwd)
   assert(fs.isdir(dir, cwd), dir .. " is not a directory.")
   assert(lfs.getInfo(realpath(dir .. "/_meta", cwd)), dir .. " has no meta.")
   return lume.deserialize(lfs.read(realpath(dir .. "/_meta", cwd)))
end

fs.hash = function(s) -- extremely insecure, but fun!
   local h = ""
   for i = 1, #s do h = h .. tostring(string.byte(string.sub(s, i, i))) end
   return h
end

fs.add_user = function(user, password)
   local home = "/home/" .. user
   fs.mkdir(home)
   fs.chown(home, "/", user, user)
   fs.mkdir(home .. "/bin", "/")
   fs.chown(home .. "/bin", "/", user, user)
   fs.add_to_group(user, user)
   fs.add_to_group(user, "all")
   fs.write("/etc/passwords/" .. user, "/",
            fs.hash(user .. ":" .. password))
end

fs.add_to_group = function(user, group)
   assert(type(user) == "string" and type(group) == "string")
   local group_dir = "/etc/groups/" .. group

   if(not lfs.getInfo(group_dir)) then
      fs.mkdir(group_dir, "/")
      fs.chown(group_dir, "/", user, user)
   end

   fs.write(group_dir .. "/" .. user, "/", "")
end

local load_bin = function()
   local files = lfs.getDirectoryItems("os/orb/resources/")
   for _,path in ipairs(files) do
      local real_path = "os/orb/resources/" .. path
      fs.write("/bin/" .. path, "/", assert(lfs.read(real_path)))
   end
end

-- Load up an empty filesystem. Provided users will be added as sudoers.
fs.seed = function(hostname, users)
   for _,d in pairs({"/", "/etc", "/home", "/tmp", "/bin"}) do
      fs.mkdir(d, "/")
      fs.chown(d, "/", "root", "all")
   end

   fs.mkdir("/etc/passwords", "/")
   fs.mkdir("/etc/groups", "/")
   fs.chown("/tmp", "/", "root", "all", true)

   for user, password in pairs(users or {}) do
      fs.add_user(user, password)
      fs.add_to_group(user, "sudoers")
   end
   if(lfs.getInfo("data/motd/" .. hostname)) then
      fs.write("/etc/motd", "/", lfs.read("data/motd/" .. hostname))
   end

   for user, data in pairs(seed_data[hostname] or {}) do
      for name,content in pairs(data.files or {}) do
         if(not fs.exists(fs.dirname(name))) then
            fs.mkdir(fs.dirname(name), "/")
            fs.chown(fs.dirname(name), user)
         end
         fs.write(name, "/", content)
      end
   end

   load_bin()
end

fs.normalize = function(path, cwd)
   if(path == ".") then return cwd end
   if(not path:match("^/")) then path = assert(cwd) .. "/" .. path end

   local final = {}
   for _,segment in pairs(lume.split(path, "/")) do
      if(segment == "..") then
         table.remove(final, #final)
      elseif(segment ~= "") then
         final[#final + 1] = segment
      end
   end

   return "/" .. table.concat(final, "/")
end

fs.in_group = function(user, group)
   return fs.isdir("/etc/groups/" .. group) and
      lfs.getInfo(realpath("/etc/groups/" .. group) .. "/" .. user)
end

fs.readable = function(f, user)
   if(user == "root") then return true end
   local info = lfs.getInfo(realpath(f)) or {}
   local dir = (info.type == "directory") and f or fs.dirname(f)
   local m = fs.dir_meta(dir)
   return m.user == nil or m.user == user or fs.in_group(user, m.group)
end

fs.writeable = function(f, user)
   if(user == "root") then return true end
   local info = lfs.getInfo(realpath(f)) or {}
   local dir = (info.type == "directory") and f or fs.dirname(f)
   local m = fs.dir_meta(dir)
   return m.user == nil or m.user == user or
      (m.group_write and fs.in_group(user, m.group))
end

fs.if_readable = function(user, f, for_dir)
   return function(path, cwd)
      local dir = for_dir and path or fs.dirname(path, cwd)
      dir = fs.normalize(dir, cwd)
      assert(fs.readable(dir, user), dir .. " is not readable by " .. user)
      return f(path, cwd)
   end
end

fs.if_writeable = function(user, f, for_dir)
   return function(path, cwd, ...)
      local dir = for_dir and path or fs.dirname(path, cwd)
      dir = fs.normalize(dir, cwd)
      assert(fs.writeable(dir, user), dir .. " is not writeable by " .. user)
      return f(path, cwd, ...)
   end
end

fs.init_if_needed = function(hostname)
   if(fs.hostname) then
      assert(hostname == fs.hostname,
             "Hostname mismatch! " .. hostname .. " / " .. fs.hostname)
   else
      fs.hostname = hostname
      if(not fs.exists("/", "/")) then
         fs.seed(hostname)
      end
      fs.add_user("guest", "")
   end
end

return fs
